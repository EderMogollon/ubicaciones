package com.example.ubicaciones

import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.example.ubicaciones.databinding.ActivityMapsBinding

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private val taquerias = mutableListOf<Taqueria>()
    private val tiendas = mutableListOf<Tienda>()
    private val escuelas = mutableListOf<Escuela>()
    private val hoteles = mutableListOf<Hotel>()


    private lateinit var myLocationButton : FloatingActionButton
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        addTaqueria()
        addTienda()
        addEscuela()
        addHotel()



        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }



    private fun addTienda() {
        tiendas.add(Tienda("El Changarrito",20.023617018755694,-96.87296806143118))
        tiendas.add(Tienda("Modelorama",20.023371977129994,-96.87326236244661))
        tiendas.add(Tienda("Ciber el Portal",20.024496952119392,-96.87326232289189))
        tiendas.add(Tienda("Biomagnetismo",20.024332612105464,-96.87271140668842))
        tiendas.add(Tienda("Miel Artesanal Gil",20.024033841424373,-96.87472180520409))
        tiendas.add(Tienda("Merceria y Regalos ABC",20.023746946174796,-96.87399350766515))
        tiendas.add(Tienda("Amor de Frida",20.021928360949858,-96.87317891896724))
        tiendas.add(Tienda("Construrama",20.0212709559784,-96.87279580537209))
        tiendas.add(Tienda("Agua Purificadora",20.023905675010337,-96.86981416933486))
    }



    private fun addTaqueria() {
        taquerias.add(Taqueria("Taqueria Estrella",19.934618297282906,-96.84890002012256))
        taquerias.add(Taqueria("Taqueria El Compa",19.931103291068656,-96.8522796034813))
        taquerias.add(Taqueria("Taqueria La Parroquia II", 19.92877335863342,-96.85337394475938))
        taquerias.add(Taqueria("Taqueria Raquel",19.928571631265534,-96.85337930917741))
        taquerias.add(Taqueria("Taqueria La Parroquia",19.928223650950734,-96.85451656579973))
        taquerias.add(Taqueria("Taqueria Reyna",20.02345058811,-96.87303452269384))
        taquerias.add(Taqueria("Antojitos Mary",20.02366381546512,-96.87306738676685))
        taquerias.add(Taqueria("Taqueria Trujillo",20.024376669609776,-96.87325943138562))
        taquerias.add(Taqueria("Delicias al Pastor",20.023200022787755,-96.87365529312238))
        taquerias.add(Taqueria("Tacos Guicho",19.93499637927574,-96.85017790184271))
        taquerias.add(Taqueria("Taqueria Elodia",19.928924141006867,-96.8536694935787))
        taquerias.add(Taqueria("Taqueria el Chilero",19.927880200093664,-96.854444651986))
        taquerias.add(Taqueria("Loncheria Almendrita",19.99935268340012,-96.88212306569294))
        taquerias.add(Taqueria("Las Cazuelas San Francisco",20.029913236974185,-96.8751210812172))

    }


    private fun addEscuela() {
        escuelas.add(Escuela("Primaria. Prof. Alfonso Arroyo Flores",20.024434521663462,-96.87484233139322))
        escuelas.add(Escuela("Primaria. Lic. Benitio Juarez",20.0247651024532,-96.87490548427142))
        escuelas.add(Escuela("Preescolar. Rosaura Zapata",20.023052897154855,-96.87452223224867))
        escuelas.add(Escuela("Telesecundaria",20.0236612225849,-96.8751013745634))
        escuelas.add(Escuela("CBTa #135",20.02651897951044,-96.87536428615728))
        escuelas.add(Escuela("Secundaria Tec. #27",20.02507302863452,-96.87882759677602))
        escuelas.add(Escuela("Telebachillerato",20.019977270651655,-96.87259940490063))

    }


    private fun addHotel() {
        hoteles.add(Hotel("Motel Los Tulipanes",19.96275594034263,-96.87337967175257))


    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val icon = getTacoIcon()
        val icon1 = getTiendaIcon()
        val icon2 = getEscuelaIcon()
        val icon3 = getHotelIcon()


        for (taqueria in taquerias){
            val taqueriaPosition = LatLng(taqueria.latitud,taqueria.longitud)
            val taqueriaName = taqueria.name

            val markerOptions = MarkerOptions().position(taqueriaPosition).title(taqueriaName)
                .icon(icon)
            mMap.addMarker(markerOptions)
        }


        for (tienda in tiendas){
            val tiendaPosition = LatLng(tienda.latitud,tienda.longitud)
            val tiendaName = tienda.name

            val markerOptions = MarkerOptions().position(tiendaPosition).title(tiendaName)
                    .icon(icon1)
            mMap.addMarker(markerOptions)
        }



        for (escuela in escuelas){
            val escuelaPosition = LatLng(escuela.latitud,escuela.longitud)
            val escuelaName = escuela.name

            val markerOptions = MarkerOptions().position(escuelaPosition).title(escuelaName)
                    .icon(icon2)
            mMap.addMarker(markerOptions)
        }



        for (hotel in hoteles){
            val hotelPosition = LatLng(hotel.latitud,hotel.longitud)
            val hotelName = hotel.name

            val markerOptions = MarkerOptions().position(hotelPosition).title(hotelName)
                    .icon(icon3)
            mMap.addMarker(markerOptions)
        }





        // Add a marker in Sydney and move the camera
        val Eder = LatLng(20.024371505589183, -96.87284209345788)
        mMap.addMarker(MarkerOptions().position(Eder).title("Mi Casa"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Eder))



        binding.myLocation.setOnClickListener {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Eder,13.0f))

        }
    }

    private fun getTacoIcon(): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, R.drawable.taco_2)
        drawable?.setBounds(0,0,drawable.intrinsicWidth,drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(drawable?.intrinsicWidth ?: 0,
                drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable?.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)

    }


    private fun getTiendaIcon(): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, R.drawable.tienda_2)
        drawable?.setBounds(0,0,drawable.intrinsicWidth,drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(drawable?.intrinsicWidth ?: 0,
                drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable?.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)

    }


    private fun getEscuelaIcon(): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, R.drawable.escuela_2)
        drawable?.setBounds(0,0,drawable.intrinsicWidth,drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(drawable?.intrinsicWidth ?: 0,
                drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable?.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)

    }


    private fun getHotelIcon(): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, R.drawable.hotel)
        drawable?.setBounds(0,0,drawable.intrinsicWidth,drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(drawable?.intrinsicWidth ?: 0,
                drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable?.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)

    }







}